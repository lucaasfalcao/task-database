import new_ox_data
import ox_fatter
import ox_thinner

def set_action():
    print('Digite o número referente à ação desejada:')
    print('1. Cadastrar um novo boi.')
    print('2. Calcular o boi mais gordo.')
    print('3. Calcular o boi mais magro.')
    print()
    action = input('Número: ')
    print()

    if action == '1':
        new_ox_data.new_ox()

    elif action == '2':
        ox_fatter.ox_fatter()
    
    elif action == '3':
        ox_thinner.ox_thinner()
    else:
        print('Erro: ação não encontrada.')

    print()
    print('Deseja realizar outra ação?')
    print('1. Sim.')
    print('2. Não.')
    print()
    another_action = input('Número: ')
    print()

    if another_action == '2':
        print('Sistema fechado!')

    elif another_action == '1':
        set_action()
    else:
        print('Erro: ação não encontrada.')

